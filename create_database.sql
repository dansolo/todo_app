DROP TABLE boards;
DROP TABLE tasks;
CREATE TABLE boards (id VARCHAR(100) NOT NULL, access_token VARCHAR(64), PRIMARY KEY (id));
CREATE TABLE tasks ( id integer PRIMARY KEY NOT NULL, title VARCHAR(100) NOT NULL, details TEXT, due DATE, x INT, y INT,board VARCHAR(100), FOREIGN KEY(board) REFERENCES boards(token));
