package main

import (
	"bytes"
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	app_root = ""

	foot = `
	</div>
	</body>
	</html>
	`

	card_format_string = `
	<div class="card %s" %s cardID="%d">
				<button class="fa fa-times close-button" type="button" cardID="%d" onclick="deleteTask(this)"></button>
				<button class="fa fa-pencil edit-button" type="button" cardID="%d" onclick="editCard(this)"></button>
        <h1 class="title">%s</h1>
        <h2 class="date">%v %v %v</h2>
        <h3 class="details">%s</h3>
  </div>
	`
)

var head string
var password_page string

var db *sql.DB
var err error

type task struct {
	id      int
	title   string
	details string
	due     time.Time
	x       int
	y       int
	board   string
}

func fetch_tasks(token string) []task {
	tasks := make([]task, 0)

	rows, err := db.Query("SELECT id,title,details,due,COALESCE(x, 0),COALESCE(y, 0),board FROM tasks WHERE board=? ORDER BY due ASC;", token)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var id int
		var title string
		var details string
		var due time.Time
		var board string
		var x int
		var y int
		if err := rows.Scan(&id, &title, &details, &due, &x, &y, &board); err != nil {
			log.Fatal(err)
		}
		tasks = append(tasks, task{id, title, details, due, x, y, board})
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	return tasks
}

func add_task(t task) {
	stmt, err := db.Prepare("INSERT INTO tasks (title,details,due,board) VALUES(?,?,?,?);")
	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(t.title, t.details, t.due, t.board)
	if err != nil {
		log.Fatal(err)
	}
}

func edit_task(id int, t task) {
	stmt, err := db.Prepare("UPDATE tasks SET title = ?, details = ?, due = ?  WHERE id=? AND board=?;")
	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(t.title, t.details, t.due, id, t.board)
	if err != nil {
		log.Fatal(err)
	}
}

func remove_task(id int, token string) {
	stmt, err := db.Prepare("DELETE FROM tasks WHERE id=? AND board=?;")
	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(id, token)
	if err != nil {
		log.Fatal(err)
	}
}

func setTaskXY(x int, y int, id int, token string) {
	stmt, err := db.Prepare("UPDATE tasks SET x = ?, y = ? WHERE id=? AND board=?")

	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(x, y, id, token)
	if err != nil {
		log.Fatal(err)
	}
}

func new_board() string {
	var count string
	err := db.QueryRow("SELECT COUNT(*) FROM boards;").Scan(&count)
	if err != nil {
		log.Fatal(err)
	}

	random := rand.Intn(100000)
	token := fmt.Sprint(count, random)

	stmt, err := db.Prepare("INSERT INTO boards (id) VALUES (?);")
	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(token)
	if err != nil {
		log.Fatal(err)
	}

	return token
}

func render_board(w http.ResponseWriter, token string) {
	tasks := fetch_tasks(token)
	var extra_class string
	var style string
	var buffer bytes.Buffer
	buffer.WriteString(head)
	for _, task := range tasks {
		formatted_details := strings.Replace(task.details, "\n", "<br>", -1)
		if task.x != 0 || task.y != 0 {
			extra_class = "custom_pos"
			style = fmt.Sprintf("style=\"top: %dpx; left: %dpx;\"", task.y, task.x)
		}
		card := fmt.Sprintf(card_format_string, extra_class, style, task.id, task.id, task.id, task.title, task.due.Day(), task.due.Month(), task.due.Year(), formatted_details)
		buffer.WriteString(card)
	}
	buffer.WriteString(foot)
	io.WriteString(w, buffer.String())
}

func password_prompt(w http.ResponseWriter) {
	io.WriteString(w, password_page)
}

func is_board_protected(token string) bool {
	var access_token sql.NullString
	db.QueryRow("SELECT access_token FROM boards WHERE id = ?;", token).Scan(&access_token)
	if access_token.Valid {
		return true
	}
	return false
}

func validate_access_token(token string, board_id string) bool {
	var access_token string
	err := db.QueryRow("SELECT access_token FROM boards WHERE id = ?;", board_id).Scan(&access_token)
	if err != nil {
		return false
	}
	if token == access_token {
		return true
	} else {
		return false
	}
}

func set_board_password(board_id string, password string) {
	stmt, err := db.Prepare("UPDATE boards SET access_token = ? WHERE id = ?;")
	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(password, board_id)
	if err != nil {
		log.Fatal(err)
	}

}

func render_existing_board(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	token := params["token"]
	if !is_board_protected(token) {
		render_board(w, token)
		return
	}
	cookie, err := r.Cookie("access_token")
	if err == nil {
		if validate_access_token(cookie.Value, token) {
			render_board(w, token)
		} else {
			password_prompt(w)
		}
	} else {
		password_prompt(w)
	}
}

func render_new_board(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("board_id")
	token := ""
	if err != nil {
		token = new_board()
	} else {
		token = cookie.Value
	}

	path := fmt.Sprint("/", token)
	http.Redirect(w, r, path, http.StatusSeeOther)
}

func add_request(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var t task
		title := r.FormValue("title")
		t.title = title
		due, err := time.Parse("2006-01-02", r.FormValue("due"))
		if err != nil {
			t.due = time.Now()
		} else {
			t.due = due
		}
		t.details = r.FormValue("details")
		t.board = r.FormValue("boardtoken")
		add_task(t)
	}
}

func edit_request(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var t task
		t.board = r.FormValue("boardtoken")
		id, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		title := r.FormValue("title")
		t.title = title
		due, err := time.Parse("2006-01-02", r.FormValue("due"))
		if err != nil {
			for _, existing_task := range fetch_tasks(t.board) {
				if existing_task.id == int(id) {
					t.due = existing_task.due
				}
			}
		} else {
			t.due = due
		}
		t.details = r.FormValue("details")
		edit_task(int(id), t)
	}
}

func set_xy_request(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		board := r.FormValue("boardtoken")
		id, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		x, err := strconv.ParseFloat(strings.Replace(r.FormValue("x"), "px", "", -1), 64)
		if err != nil {
			log.Fatal(err)
		}
		y, err := strconv.ParseFloat(strings.Replace(r.FormValue("y"), "px", "", -1), 64)
		if err != nil {
			log.Fatal(err)
		}
		setTaskXY(int(x), int(y), int(id), board)
	}
}

func delete_request(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		id, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		token := r.FormValue("boardtoken")
		remove_task(int(id), token)
	}
}

func set_pass_request(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		boardid := r.FormValue("board_id")
		if is_board_protected(boardid) {
			io.WriteString(w, "Board is already password protected. You cannot set a new password.")
		} else {
			set_board_password(boardid, r.FormValue("password"))
		}
	}
}

func main() {

	rand.Seed(time.Now().UnixNano())

	head_bytes, err := ioutil.ReadFile("head.html")
	if err != nil {
		log.Fatal(err)
	}
	head = string(head_bytes)

	pass_bytes, err := ioutil.ReadFile("login.html")
	if err != nil {
		log.Fatal(err)
	}
	password_page = string(pass_bytes)

	db, err = sql.Open("sqlite3", "./todo_list.db")
	if err != nil {
		log.Fatal(err)
	}

	static := http.FileServer(http.Dir("./static"))

	rtr := mux.NewRouter()
	rtr.HandleFunc(app_root+"/", render_new_board)
	rtr.HandleFunc(app_root+"/add", add_request)
	rtr.HandleFunc(app_root+"/set_task_position", set_xy_request)
	rtr.HandleFunc(app_root+"/edit", edit_request)
	rtr.HandleFunc(app_root+"/setpass", set_pass_request)
	rtr.HandleFunc(app_root+"/delete", delete_request)
	rtr.PathPrefix(app_root + "/static").Handler(http.StripPrefix("/static/", static))
	rtr.HandleFunc(app_root+"/{token:[0-9]+}", render_existing_board)
	http.Handle(app_root+"/", rtr)
	http.ListenAndServe(":8080", nil)

	db.Close()
}
